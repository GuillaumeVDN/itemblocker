package com.guillaumevdn.itembloquer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.guillaumevdn.itembloquer.util.ConfigItem;
import com.guillaumevdn.itembloquer.util.Utils;

public class Main extends JavaPlugin {

	// instance
	private static Main instance;

	public Main() {
		instance = this;
	}

	public static Main inst() {
		return instance;
	}

	// base
	private Map<Material, List<ConfigItem>> configItems = new HashMap<Material, List<ConfigItem>>();

	// get
	public Map<Material, List<ConfigItem>> getConfigItems() {
		return configItems;
	}

	public ConfigItem getConfigItem(ItemStack item) {
		List<ConfigItem> list = configItems.get(item.getType());
		if (list != null) {
			for (ConfigItem configItem : list) {
				if ((configItem.getUnbreakable() ? Utils.isUnbreakable(item) : true) && (configItem.getDurability() == -1 ? true : configItem.getDurability() == item.getDurability())) {
					return configItem;
				}
			}
		}
		return null;
	}

	// enable
	@Override
	public void onEnable() {
		// load config
		saveDefaultConfig();
		reloadConfig();
		int loaded = 0;
		ConfigurationSection section = getConfig().getConfigurationSection("items");
		if (section != null) {
			for (String key : section.getKeys(false)) {
				try {
					Material type = Material.getMaterial(section.getString(key + ".type", null));
					int durability = section.getInt(key + ".durability", -1);
					boolean unbreakable = section.getBoolean(key + ".unbreakable", false);
					if (type == null || durability < -1) throw new Error("invalid settings (target " + type + ", durability " + durability + ")");
					List<ConfigItem> list = configItems.get(type);
					if (list == null) configItems.put(type, list = new ArrayList<ConfigItem>());
					list.add(new ConfigItem(type, durability, unbreakable));
					++loaded;
				} catch (Throwable exception) {
					exception.printStackTrace();
					Bukkit.getLogger().severe("[" + getName() + "] Couldn't load config item with key " + key);
				}
			}
		}
		Bukkit.getLogger().info("[" + getName() + "] Loaded " + loaded + " config item" + (loaded > 1 ? "s" : ""));
		// register listeners
		Bukkit.getPluginManager().registerEvents(new Listeners(), this);
	}

	// disable
	@Override
	public void onDisable() {
	}

}
